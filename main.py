#==========================
# Bibliotheken
#==========================

import datetime
import hashlib
import json
import sqlite3
from multiprocessing import Process
from flask import Flask, request, session, redirect, render_template
from numpy import interp
from time import sleep
try:
	import Adafruit_ADS1x15
except ImportError:
	print("Not a Pi oder missing lib (Import Error)")

#==========================
# Pflanzen-Funktionen
#==========================

#Sonne steuern
##############

class Helios:

	# initialisieren
	def __init__(self, db):
		self.sunpin = 0
		self.db = db
		self.con = db.cursor()
		self.sunh = 0
		self.sunrise = 0
		self.sunset = 0

	# nötige Infos aus Datenbank abrufen
	def get_data(self):
		self.con.execute('SELECT sunh FROM helios WHERE id=1')
		xrow = self.con.fetchone()
		self.sunh = xrow['sunh']

	# anhand von Tageslänge Timestamp für Sonnenauf-/untergang berechnen
	def set_day(self):
		riseh = 12 - int(self.sunh/2)
		seth = 12 + int(self.sunh/2)
		dtnow = datetime.datetime.now()
		self.sunrise = datetime.datetime(dtnow.year, dtnow.month, dtnow.day, riseh, 0, 0)
		self.sunset = datetime.datetime(dtnow.year, dtnow.month, dtnow.day, seth, 0, 0)

	# Daten exportieren
	def data_to_db(self):
		dtnow = datetime.datetime.now()
		if self.sunrise.timestamp() <= dtnow.timestamp() < self.sunset.timestamp():
			t1 = 1
			t2 = self.sunset.timestamp()
			print("First clause")
		else:
			t1 = 0
			if dtnow.timestamp() < self.sunrise.timestamp():
				t2 = self.sunrise.timestamp()
				print("Second clause")
			else:
				t = self.sunrise + datetime.timedelta(days=1)
				t2 = t.timestamp()
				print("Third clause")
		self.con.execute("UPDATE helios SET isDay=?, nextChange=? WHERE id=1", (t1, t2))
		self.db.commit()

	# alles zusammenhängen: Sonne ein- und ausschalten
	def solar_chariot(self):
		self.get_data()
		self.set_day()
		self.data_to_db()


# Kühlung steuern
#################

class SolarWind:

	def __init__(self, db, rid):
		self.db = db
		self.con = self.db.cursor()
		self.rid = rid
		self.coldThresh = 0
		self.hotThresh = 0
		self.critThresh = 0
		self.lpin = 0
		self.fpin = 0
		self.is_day = 0
		self.is_active = 0
		self.temp_reading = 100.0
		self.get_data()
		self.hw_setup()

	def get_data(self):
		self.con.execute('SELECT isDay, coldThresh, hotThresh, critThresh FROM helios WHERE id=1')
		xrow = self.con.fetchone()
		self.con.execute('SELECT lpin, fpin, active FROM solarwind WHERE id=?', (self.rid,))
		yrow = self.con.fetchone()
		self.is_day = xrow['isDay']
		self.coldThresh = xrow['coldThresh']
		self.hotThresh = xrow['hotThresh']
		self.critThresh = xrow['critThresh']
		self.lpin = yrow['lpin']
		self.fpin = yrow['fpin']
		self.is_active = yrow['active']

	def hw_setup(self):
		#TODO: GPIO-Setup für lpin und fpin
		pass

	def read_data(self):
		#TODO: Sensor tatsächlich lesen
		self.temp_reading = 50
		self.con.execute('UPDATE solarwind SET readtemp=? WHERE id=?', (self.temp_reading, self.rid))
		self.db.commit()

	def switch_fan(self):
		if self.temp_reading >= self.coldThresh:
			fan_speed = int(interp(self.temp_reading, [self.coldThresh, self.hotThresh], [10, 100]))
		else:
			fan_speed = 0
		self.con.execute('UPDATE solarwind SET fanspeed=? WHERE id=?', (fan_speed, self.rid))
		self.db.commit()
		#TODO: self.fpin pwmoduliert mit fan_speed einschalten

	def switch_sun(self):
		self.con.execute('SELECT overheat FROM solarwind WHERE id=?', (self.rid,))
		xrow = self.con.fetchone()
		is_overheated = xrow['overheated']
		self.get_data()
		self.read_data()
		self.switch_fan()
		if self.is_day and self.is_active:
			if (self.temp_reading < self.critThresh and is_overheated == 0) or (is_overheated == 1 and self.temp_reading <= self.coldThresh):
				self.con.execute('UPDATE solarwind SET overheat=0 WHERE id=?', (self.rid,))
				self.db.commit()
				#TODO: GPIO lpin einschalten

			else:
				self.con.execute('UPDATE solarwind SET overheat=1 WHERE id=?', (self.rid,))
				self.db.commit()
				#TODO: GPIO lpin ausschalten

		else:
			#TODO: GPIO lpin ausschalten
			pass

# Regen steuern
###############

class RainControl:

	# initialisieren
	def __init__(self, db, rid, adc):
		self.apin = 0
		self.ppin = 0
		self.db = db
		self.con = db.cursor()
		self.rid = rid
		self.reading = 0
		self.lowThresh = 0
		self.highThresh = 0
		self.dryDelta = 0
		self.dryStamp = 0
		self.waterLow = 0
		self.adc = adc
		self.get_data() # Pin-Nummern für apin, ppin speichern vor HW-Setup
		#TODO: Setup für ppin

	# nötige Infos aus Datenbank abrufen
	def get_data(self):
		t = (self.rid, )
		self.con.execute("SELECT ppin, apin, lowThresh, highThresh, dryDelta, dryStamp FROM rainfall WHERE id=?", t)
		data = self.con.fetchone()
		#print(dict(data))
		self.ppin = int(data['ppin'])
		self.apin = int(data['apin'])
		self.lowThresh = data['lowThresh']
		self.highThresh = data['highThresh']
		self.dryDelta = data['dryDelta']
		self.dryStamp = data['dryStamp']

	def check_level(self):
		self.con.execute("SELECT waterSensorPin FROM helios WHERE id=1")
		data = self.con.fetchone()
		pin_to_check = int(data['waterLevelPin'])
		#TODO: Messung von pin_to_check
		self.waterLow = 0
		###
		self.con.execute("UPDATE helios SET waterLow=? WHERE id=1", self.waterLow)
		self.db.commit()

	# Sensoren abfragen, in Datenbank schreiben
	def read_data(self):
		#DONE: Wirkliche Messung von ADC
		raw_reading = self.adc.read_adc(self.apin, gain=1)
		self.reading = interp(raw_reading, [0, 32767], [0, 100])
		t = (self.reading, self.rid)
		self.con.execute("UPDATE rainfall SET reading=? WHERE id=?", t)
		self.db.commit()
		return self.reading

	# Regen starten
	def start_rain(self):
		if not self.waterLow:
			#TODO: GPIO ppin einschalten
			pass
		t = (self.rid, )
		self.con.execute("UPDATE rainfall SET dryStamp=-1 WHERE id=?", t)
		self.db.commit()

	# Regen stoppen
	def stop_rain(self):
		#TODO: GPIO ppin ausschalten
		t = (self.rid, )
		self.con.execute("UPDATE rainfall SET dryStamp=0 WHERE id=?", t)
		self.db.commit()

	# Trockenzeit starten
	def dry_out(self):
		dtnow = datetime.datetime.now()
		dtStamp = dtnow.timestamp() + self.dryDelta * 3600
		t = (dtStamp, self.rid)
		self.con.execute("UPDATE rainfall SET dryStamp=? WHERE id=?", t)
		self.db.commit()

	# Alles zusammenhängen: Regen steuern
	def rain_check(self):
		self.get_data()
		self.read_data()
		self.check_level()
		dtnow = datetime.datetime.now()

		if self.dryStamp == -1 and self.reading >= self.highThresh:
			self.stop_rain()

		elif self.dryStamp == 0 and self.reading <= self.lowThresh:
			self.dry_out()

		elif self.dryStamp >= dtnow.timestamp():
			self.start_rain()


class CheckCritical:

	def __init__(self, db):
		self.db = db
		self.con = db.cursor()
		self.waterLow = 0
		self.waterLEDPin = 0
		self.overheat = {}
		self.hw_setup()

	def hw_setup(self):
		self.con.execute('SELECT waterLEDPin FROM helios WHERE id=1')
		xrow = self.con.fetchone()
		self.waterLEDPin = xrow['waterLEDPin']
		#TODO: Setup waterLEDPin as output
		for xrow in self.con.execute('SELECT opin FROM solarwind ORDER BY id'):
			#TODO: Setup xrow['opin'] as output
			self.overheat[xrow['opin']] = 0

	def run_check(self):
		self.con.execute('SELECT waterLow FROM helios WHERE id=1')
		xrow = self.con.fetchone()
		if self.waterLow != xrow['waterLow']:
			self.waterLow = xrow['waterLow']
			#ADDON: xmpp-Msg
		self.blink(int(self.waterLEDPin), xrow['waterLow'])
		for xrow in self.con.execute('SELECT id, opin, overheat FROM solarwind ORDER BY id'):
			if self.overheat[xrow['opin']] != xrow['overheat']:
				self.overheat[xrow['opin']] = xrow['overheat']
				#ADDON: xmpp-Msg
			self.blink(int(xrow['opin']), xrow['overheat'])

	def blink(self, pin, state):
		jetzt = int(datetime.datetime.now().timestamp())
		if state == 1:
			setState = jetzt % 2
			#TODO: Set pin to setState
		else:
			#TODO: Set pin to low
			pass



#==========================
# Flask API
#==========================

# Helfer-Klasse Web Interface
##############################

class DBInterface:

	def __init__(self, db):
		self.db = db
		self.con = db.cursor()
		self.re_dict = {}

	def get_helios(self):
		self.con.execute('SELECT * FROM helios WHERE id=1')
		xrow = self.con.fetchone()
		self.re_dict['helios'] = dict(xrow)

	def get_solar(self):
		self.re_dict['solar'] = []
		for xrow in self.con.execute('SELECT * FROM solarwind ORDER BY id'):
			self.re_dict['solar'].append(dict(xrow))

	def get_rain(self):
		self.re_dict['rain'] = []
		for xrow in self.con.execute('SELECT * FROM rainfall ORDER BY id'):
			self.re_dict['rain'].append(dict(xrow))

	def get_hw(self):
		self.re_dict['hw'] = []
		for xrow in self.con.execute('SELECT * FROM wiring ORDER BY id'):
			self.re_dict['hw'].append(dict(xrow))

	def serialize_data(self):
		self.get_helios()
		self.get_solar()
		self.get_rain()
		self.get_hw()
		return json.dumps(self.re_dict)

	def receive_sun(self, form_data):
		self.con.execute('UPDATE helios SET sunh=? WHERE id=1', (form_data['sunh'],))
		self.db.commit()

	def receive_solarwind(self, form_data, rowid):
		try:
			is_active = form_data['active']
		except KeyError:
			is_active = 0
		self.con.execute('UPDATE solarwind SET active=? WHERE id=?', (is_active, rowid))
		self.db.commit()

	def receive_rain(self, form_data, rowid):
		try:
			is_active = form_data['active']
		except KeyError:
			is_active = 0
		self.con.execute('UPDATE rainfall SET active=?, lowThresh=?, highThresh=?, dryDelta=? WHERE id=?', (is_active, form_data['lowThresh'], form_data['highThresh'], form_data['dryDelta'], rowid))
		self.db.commit()

	def receive_hw(self, form_data, data_type, rowid):
		if data_type == "solar":
			self.con.execute('UPDATE solarwind SET lpin=?, fpin=?, sensorid=? WHERE id=?', (form_data['lpin'], form_data['fpin'], form_data['sensorid'], rowid))
			self.db.commit()

		elif data_type == "rain":
			self.con.execute('UPDATE rainfall SET apin=?, ppin=?, pwm=? WHERE id=?', (form_data['apin'], form_data['ppin'], form_data['pwm'], rowid))
			self.db.commit()

		elif data_type == "helios":
			self.con.execute('UPDATE helios SET coldThresh=?, hotThresh=?, critThresh=? WHERE id=1', (form_data['coldThresh'], form_data['hotThresh'], form_data['critThresh']))
			self.db.commit()

	def receive_pwd(self, form_data):
		old_hash = str(hashlib.sha256(form_data['old_pwd'].encode()).hexdigest())
		self.con.execute('SELECT passwdHash, id FROM passwds WHERE passwdHash=?', (old_hash,))
		db_data = self.con.fetchone()
		try:
			id_to_change = db_data['id']
			try:
				if db_data['passwdHash'] == old_hash and form_data['new_pwd_1'] == form_data['new_pwd_2'] and len(form_data['new_pwd_1']) > 4:
					new_hash = str(hashlib.sha256(form_data['new_pwd_1'].encode()).hexdigest())
					self.con.execute("UPDATE passwds SET passwdHash=? WHERE id=?", (new_hash, id_to_change))
					self.db.commit()
					return True
				else:
					print("Wrong: Password Check")
					return False
			except TypeError:
				print("Wrong: try Password Check")
				return False
		except TypeError or KeyError:
			print("Wrong: access db_data id")
			return False

	def check_pwd(self, pwd_to_chk):
		pwd_hash = str(hashlib.sha256(pwd_to_chk.encode()).hexdigest())
		self.con.execute('SELECT passwdHash FROM passwds WHERE passwdHash=?', (pwd_hash,))
		try:
			if self.con.fetchone()[0] == pwd_hash:
				return True
			else:
				return False
		except TypeError:
			return False


# Flask-Objekt
###############
app = Flask(__name__)
app.secret_key = "Towelie wandered off again"

# Routes
#########

# index/document root
@app.route('/')
def root():
	return redirect('/index')


@app.route('/index')
def static_proxy():
	#print(session)
	if 'loggedin' in session:
		#return app.send_static_file(path)
		serial_data = dbHandle.serialize_data()
		return render_template("skelett.html", data=json.loads(serial_data))
	else:
		return redirect('/login')


# assets ohne Login verfügar
@app.route('/assets/<path:path>')
def asset_proxy(path):
	return app.send_static_file('assets/' + path)


# Login-Funktion
@app.route('/login', methods=['POST', 'GET'])
def login():
	if request.method == 'GET':
		return app.send_static_file('login.html')
	elif request.method == 'POST':
		if dbHandle.check_pwd(request.form['pwd']):
			session['loggedin'] = True
			return redirect('/index')
		else:
			sleep(1)
			return '<script type="text/javascript">alert("Password incorrect"); window.history.back();</script>'


# Logout-Funktion
@app.route('/logout')
def logout():
	session.clear()
	return redirect('/login')


# API-Funktion
@app.route('/api/<path:path>', methods=['POST', 'GET'])
def api_function(path):

	if 'loggedin' in session:

		cmd = path.split("/")

		# Daten von Python -> JS
		if cmd[0] == 'get':
			return dbHandle.serialize_data()

		# Daten von JS -> Python
		elif cmd[0] == 'post':
			#return cmd[1]

			# Sonnen-Daten
			if cmd[1] == 'sun':
				dbHandle.receive_sun(request.form)
				#print(dict(request.form))
				return "<span class='fbOk'>Data received, telling Helios...</span>"

			# Sonnen-Daten
			if cmd[1] == 'solarwind':
				dbHandle.receive_solarwind(request.form, cmd[2])
				#print(dict(request.form))
				return "<span class='fbOk'>Data received, telling Helios...</span>"

			# Wetter-Daten
			elif cmd[1] == 'rain':
				dbHandle.receive_rain(request.form, cmd[2])
				return "<span class='fbOk'>Data received, changing the weather...</span>"

			# Hardware-Daten
			elif cmd[1] == 'hw':
				#print(request.form)
				dbHandle.receive_hw(request.form, cmd[2], cmd[3])
				return "<span class='fbOk'>Data received, rewiring stuff...</span>"

			elif cmd[1] == 'pwd':
				if dbHandle.receive_pwd(request.form):
					return "<span class='fbOk'>Data received, changing the locks...</span>"
				else:
					return "<span class='fbError'>Something went wrong.</span>"

			# URL ungültig
			else:
				return "<span class='fbError'>URL invalid</span>"

		# URL ungültig
		else:
			return "<span class='fbError'>URL invalid</span>"

	# nicht eingeloggt:
	else:
		return "access denied"


def run_flask():
	app.run(host='0.0.0.0')


#==========================
# Main
#==========================

# SETUP
########

# Datenbank initialisieren
sql = sqlite3.connect('ceres.db')
sql.row_factory = sqlite3.Row
c = sql.cursor()

# ADC initialisieren
try:
	ads1115 = Adafruit_ADS1x15.ADS1115(address=0x48, busnum=1)
except NameError:
	ads1115 = None
	print("Maybe not a Pi: ADC-Object not created")

# Tageszeit-Objekt
helios = Helios(sql)

# LED/Fan-Objekte
solarwind = []
for row in c.execute('SELECT id FROM solarwind ORDER BY id'):
	solarwind.append(SolarWind(sql, row[0]))

# Regen-Objekte
rainfall = []
for row in c.execute('SELECT id FROM rainfall ORDER BY id'):
	rainfall.append(RainControl(sql, row[0], ads1115))

# Kritische Werte prüfen
crit = CheckCritical(sql)

# Datenbank-Objekt für Flask
dbHandle = DBInterface(sql)

# Flask starten
flaskThread = Process(target=run_flask)
flaskThread.start()


# TESTING
##########
helios.solar_chariot()
print(dbHandle.serialize_data())


# MAIN LOOP
###########
'''
while True:
	helios.solar_chariot()
	for i in solarwind:
		i.switch_sun()
	for i in rainfall:
		i.rain_check()
	crit.run_check()
'''
