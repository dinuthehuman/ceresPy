//===================================
// Formulare nur per AJAX verschicken
//===================================
$(function() {
    $("form").on("submit", function(e) {
        e.preventDefault();
        $.ajax({
            url: $(this).attr("action"),
            type: 'POST',
            data: $(this).serialize(),
            beforeSend: function() {
                $("#fbText").html("sending...");
                $("#fbCon").fadeIn(500);
            },
            success: function(data) {
                //$("#message").hide();
                $("#fbText").html(data);
            },
            error: function() {
                $("#fbText").html("<span class='fbError'>AJAX-Error</span>");
            },
            complete: function() {
                setTimeout(function() {
                    $("#fbCon").fadeOut(500);
                }, 3000);
            }
        });
    });
});


//===================================
// Label für input range
//===================================
$(function() {
    $("input[type=range]").on('change', function() {
        console.log(this.value);
        console.log($(this).parent().children('.rangeLabel'));
        $(this).parent().children('.rangeLabel').html(this.value);    
    }); 
});


//===================================
// jQuery UI: Tabs initilisieren
//===================================
$(function() {
    $("#tabs").tabs();
});

//===================================
// Grösse steuern: Server Feedback
//===================================
$(function () {
    $("#fbCon").outerWidth($("body:first").outerWidth(true));  
});
$(window).resize(function(){
    $("#fbCon").outerWidth($("body:first").outerWidth(true));
});


//===================================
// AJAX: Interface mit Daten füllen
//===================================


$(function(){
    updateData();    
});
setInterval(updateData, 10000);

function updateData() {
    $.ajax({
        url: '/api/get',
        type: 'GET',
        success: function(json_string) {
            json_object = JSON.parse(json_string);
            updateSun(json_object);
            updateSolarwind(json_object);
            updateRain(json_object);
            updateCrit(json_object);
        }
    });
}

function updateSun(json_data) {
    if(json_data.helios.isDay == 1) {
        $("#isDay").html("Tag");
        $("#rise_set").html("Sonnenuntergang");
    }
    else {
        $("#isDay").html("Nacht");
        $("#rise_set").html("Sonnenaufgang");
    }
    initializeClock("sun_countdown", json_data.helios.nextChange);
}

function updateSolarwind(json_data) {
    for(var i=0; i < json_data.solar.length; i++) {
        $("#solartemp_" + json_data.solar[i].id).val(json_data.solar[i].readtemp);
        $("#solartemp_label_" + json_data.solar[i].id).html(json_data.solar[i].readtemp);
    }
}

function updateRain(json_data) {
    for(var i=0; i < json_data.rain.length; i++) {
        $("#rain_reading_" + json_data.rain[i].id).val(json_data.rain[i].reading);
        $("#rain_reading_label_" + json_data.rain[i].id).html(json_data.rain[i].reading);
        
        if(json_data.rain[i].dryStamp == -1) {
            $("#dryMsg_" + json_data.rain[i].id).html("Regenzeit");
            $("#dry_countdown_wrapper_" + json_data.rain[i].id).hide();
        }
        else if(json_data.rain[i].dryStamp == 0) {
            $("#dryMsg_" + json_data.rain[i].id).html("Post-Regenzeit");
            $("#dry_countdown_wrapper_" + json_data.rain[i].id).hide();
        }
        else {
            $("#dryMsg_" + json_data.rain[i].id).html("Trockenzeit");
            initializeClock("dry_countdown_" + json_data.rain[i].id, json_data.rain[i].dryStamp);
            $("#dry_countdown_wrapper_" + json_data.rain[i].id).show();
        }
    }
}

function updateCrit(json_data) {
    errors_found = 0;
    $("div#critical div").html("");
    for(var i=0; i < json_data.solar.length; i++) {
        if(json_data.solar[i].overheat == 1) {
            $("div#critical div").append("<span class='crit'>LED " + json_data.solar[i].id + " überhitzt - aktuelle Temperatur: " + json_data.solar[i].readtemp + " °C</span>");
            errors_found++;
        }
    }
    if(json_data.helios.waterLow == 1) {
        $("div#critical div").append("<span class='crit'>Wasserlevel kritisch</span>");
        errors_found++;
    }
    if(errors_found === 0) {
        $("div#critical div").append("<span class='not_crit'>Alles in Ordnung.</span>");
    }
    //$("div#critical div").html(returnString);
    //console.log(returnString);
}



//===================================
// Funktionen für Countdown
//===================================

function initializeClock(id, endtime) {
    var clock = document.getElementById(id);
    var hoursSpan = clock.querySelector('.hours');
    var minutesSpan = clock.querySelector('.minutes');
    var secondsSpan = clock.querySelector('.seconds');
    
    function updateClock() {
        var t = getTimeRemaining(endtime);
        
        hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
        minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
        secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);
        
        if (t.total <= 0) {
            clearInterval(timeinterval);
        }
    }

    updateClock();
    var timeinterval = setInterval(updateClock, 1000);
}

function getTimeRemaining(endtime){
    var t = endtime -  Math.floor(Date.now() / 1000);
    var seconds = Math.floor(t % 60);
    var minutes = Math.floor((t/60) % 60);
    var hours = Math.floor((t/(60*60)) % 24 );
    return {
        'total': t,
        'hours': hours,
        'minutes': minutes,
        'seconds': seconds
    };
}
