from time import sleep
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(15, GPIO.OUT)
led = GPIO.PWM(15, 100)
led.start(0)
while True:
	for i in range(0, 100):
		led.ChangeDutyCycle(i)
		print("set Cycle to %d" % i)
		sleep(0.05)
	for i in range(0, 100):
		j = 100 -i
		led.ChangeDutyCycle(j)
		print("set cycle to %d" % j)
		sleep(0.05)
