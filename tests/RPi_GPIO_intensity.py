from time import sleep
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(15, GPIO.OUT)
led = GPIO.PWM(15, 100)
led.start(0)

while True:
	led.ChangeDutyCycle(50)
	sleep(5)
	led.ChangeDutyCycle(100)
	sleep(5)
